#
# Be sure to run `pod lib lint DreamrCalendar.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'DreamrCalendar'
  s.version          = '1.0.1'
  s.summary          = 'Simple reuseable calendar'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
Simple reuseable calendar with customisable font and selection colours. The calendar presents date horizontally and time vertically using a gesture based navigation.
                       DESC

  s.homepage         = 'https://gitlab.com/internal.dreamr.uk/DreamrCalendar'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Ted' => 'tl@dreamr.uk' }
  s.source           = { :git => 'https://gitlab.com/internal.dreamr.uk/DreamrCalendar.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

  s.source_files = 'DreamrCalendar/Classes/**/*'

#s.resource_bundles = {
#   'DreamrCalendarAssets' => ['DreamrCalendar/*.png']
# }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
