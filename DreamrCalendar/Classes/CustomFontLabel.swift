//
//  CustomFontLabel.swift
//  Pods
//
//  Created by Work on 03/08/2016.
//
//

import UIKit

class CustomFontLabel: UILabel {
    override func drawTextInRect(rect: CGRect) {
        let pointSize = self.font.pointSize
        let newRect = CGRectMake(rect.origin.x, rect.origin.y + (0.15 * pointSize), rect.size.width, rect.size.height)
        super.drawTextInRect(newRect)
    }
}
