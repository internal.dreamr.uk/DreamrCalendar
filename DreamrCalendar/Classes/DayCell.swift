//
//  DayCell.swift
//  DreamrCalendar
//
//  Created by Work on 08/07/2016.
//  Copyright © 2016 CocoaPods. All rights reserved.
//

import UIKit

class DayCell: UICollectionViewCell {

    @IBOutlet weak var dayOfWeekLbl: CustomFontLabel!
    @IBOutlet weak var dayOfMonthLbl: CustomFontLabel!

    func refresh(date: NSDate, info: DreamrCalendarInfo?, selected: Bool) {
        
        let calendar = NSCalendar.currentCalendar()
        let components = calendar.components(.Day, fromDate: date)
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "E"
        let dayOfWeek = dateFormatter.stringFromDate(date)
        
        let numberFormatter = NSNumberFormatter()
        numberFormatter.minimumIntegerDigits = 2
        let dayOfMonth = numberFormatter.stringFromNumber(components.day)
        
        self.dayOfWeekLbl.text = dayOfWeek
        if let appearanceInfo = info?.dateAppearanceInfo {
            self.dayOfWeekLbl.textColor = appearanceInfo.deselectedTextColor
        }
        if let fontName = info?.fontPostScriptName {
            self.dayOfWeekLbl.font = UIFont(name: fontName, size: 11.0)!
        }
        
        self.dayOfMonthLbl.text = dayOfMonth
        if let appearanceInfo = info?.dateAppearanceInfo {
            self.dayOfMonthLbl.textColor = appearanceInfo.deselectedTextColor
        }
        if let fontName = info?.fontPostScriptName {
            self.dayOfMonthLbl.font = UIFont(name: fontName, size: 15.0)
        }
        
        if let appearanceInfo = info?.dateAppearanceInfo where selected {
            self.setSelected(appearanceInfo)
        }
    }
    
    func setSelected(appearanceInfo: DreamrCalendarAppearanceInfo) {
        self.dayOfWeekLbl.textColor = appearanceInfo.selectedTextColor
        self.dayOfMonthLbl.textColor = appearanceInfo.selectedTextColor
    }
    func setDeselected(appearanceInfo: DreamrCalendarAppearanceInfo) {
        self.dayOfWeekLbl.textColor = appearanceInfo.deselectedTextColor
        self.dayOfMonthLbl.textColor = appearanceInfo.deselectedTextColor
    }
}
