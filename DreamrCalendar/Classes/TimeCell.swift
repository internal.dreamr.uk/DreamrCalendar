//
//  TimeCell.swift
//  Pods
//
//  Created by Work on 03/08/2016.
//
//

import UIKit

class TimeCell: UITableViewCell {

    @IBOutlet weak var timeLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let div = UIView()
        div.backgroundColor = UIColor(red: 208/255, green: 208/255, blue: 208/255, alpha: 1.0)
        div.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(div)
        
        let views = ["div": div]
        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-22-[div]-22-|", options: [], metrics: nil, views: views))
        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:[div(0.5)]|", options: [], metrics: nil, views: views))
        self.layoutIfNeeded()
        self.backgroundColor = UIColor.clearColor()
    }
    
    func refresh(date: NSDate, info: DreamrCalendarInfo?) {
                
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        let time = dateFormatter.stringFromDate(date)
        self.timeLbl.text = time
        if let appearanceInfo = info?.dateAppearanceInfo {
            self.timeLbl.textColor = appearanceInfo.deselectedTextColor
        }
        if let fontName = info?.fontPostScriptName {
            self.timeLbl.font = UIFont(name: fontName, size: 12.0)!
        }
    }
    
    func setSelected(selected: Bool, appearanceInfo: DreamrCalendarAppearanceInfo, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        if selected {
            self.timeLbl.textColor = appearanceInfo.selectedTextColor
        } else {
            self.timeLbl.textColor = appearanceInfo.deselectedTextColor
        }
    }
}
