//
//  DreamrCalendarView.swift
//  DreamrCalendar
//
//  Created by Work on 08/07/2016.
//  Copyright © 2016 CocoaPods. All rights reserved.
//

import UIKit

public protocol DreamrCalendarViewDelegate: class {
    func didSelectDateItem(indexPath: NSIndexPath, cell: UICollectionViewCell?, date: NSDate)
    func didSelectTimeRow(indexPath: NSIndexPath, cell: UITableViewCell?, dateTime: NSDate)
}

public struct DreamrCalendarAppearanceInfo {
    var selectedTextColor: UIColor
    var deselectedTextColor: UIColor
    public init(selectedTextColor: UIColor, deselectedTextColor: UIColor) {
        self.selectedTextColor = selectedTextColor
        self.deselectedTextColor = deselectedTextColor
    }
}

public struct DreamrCalendarInfo {
    var dateAppearanceInfo: DreamrCalendarAppearanceInfo
    var timeAppearanceInfo: DreamrCalendarAppearanceInfo
    var fontPostScriptName: String
    public init(dateAppearanceInfo: DreamrCalendarAppearanceInfo, timeAppearanceInfo: DreamrCalendarAppearanceInfo, fontPostScriptName: String) {
        self.dateAppearanceInfo = dateAppearanceInfo
        self.timeAppearanceInfo = timeAppearanceInfo
        self.fontPostScriptName = fontPostScriptName
    }
}

public typealias CalendarRange = (from: NSDate, to: NSDate, interval: Int)

public class DreamrCalendarView: UIView {

    var collectionView: CalendarCollectionView!
    var tableView: CalendarTableView!
    var dateRange: CalendarRange
    var timeRange: CalendarRange
    public var delegate: DreamrCalendarViewDelegate?
    var info: DreamrCalendarInfo?
    
    //MARK: - Init
    
    public init(dateRange: CalendarRange, timeRange: CalendarRange, info: DreamrCalendarInfo?) {
        self.dateRange = dateRange
        self.timeRange = timeRange
        self.info = info
        super.init(frame: CGRectZero)
        self.addCollectionView()
        self.addTableView()
        self.setAppearance()
    }
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Appearance
    
    func setAppearance() {
        self.backgroundColor = UIColor.clearColor()
    }
    
    //MARK: - CollectionView
    
    //MARK: Add
    func addCollectionView() {
        self.collectionView = CalendarCollectionView(dateRange: self.dateRange, info: self.info)
        self.collectionView.translatesAutoresizingMaskIntoConstraints = false
        self.collectionView.calendarDelegate = self
        self.addSubview(self.collectionView)
        let views = ["collectionView": self.collectionView]
        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-12-[collectionView]-12-|", options: [], metrics: nil, views: views))
        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-12-[collectionView(45.0)]", options: [], metrics: nil, views: views))
    }
    
    //MARK: - TableView
    
    //MARK: Add
    func addTableView() {
        self.tableView = CalendarTableView(timeRange: self.timeRange, info: self.info)
        self.tableView.translatesAutoresizingMaskIntoConstraints = false
        self.tableView.calendarDelegate = self
        self.addSubview(self.tableView)
        let views = ["tableView": self.tableView, "collectionView": self.collectionView]
        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[tableView]|", options: [], metrics: nil, views: views))
        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:[collectionView][tableView]|", options: [], metrics: nil, views: views))
    }
}

extension DreamrCalendarView: CalendarCollectionViewDelegate {
    func didSelectItem(indexPath: NSIndexPath, cell: UICollectionViewCell?, date: NSDate) {
        self.delegate?.didSelectDateItem(indexPath, cell: cell, date: date)
    }
}

extension DreamrCalendarView: CalendarTableViewDelegate {
    func didSelectRow(indexPath: NSIndexPath, cell: UITableViewCell?, time: NSDate) {
        print(self.collectionView.indexPathsForSelectedItems())
        guard let indexPaths = self.collectionView.indexPathsForSelectedItems() where indexPaths.count > 0 else {
            return
        }
        let indexPath = indexPaths[0]
        guard let dates = self.collectionView.dates[self.collectionView.keys[indexPath.section]], let combinedDate = self.combine(dates[indexPath.row], withTime: time) else {
            return
        }
        self.delegate?.didSelectTimeRow(indexPath, cell: cell, dateTime: combinedDate)
    }
    func getComponents(fromDate date: NSDate) -> NSDateComponents {
        let unitFlags: NSCalendarUnit = [.Second, .Minute, .Hour, .Day, .Month, .Year]
        return NSCalendar.currentCalendar().components(unitFlags, fromDate: date)
    }
    func combine(date: NSDate, withTime time: NSDate) -> NSDate? {
        // combine dates so that time has correct year, month and day
        let dateComponents = self.getComponents(fromDate: date)
        let timeComponents = self.getComponents(fromDate: time)
        timeComponents.year = dateComponents.year
        timeComponents.month = dateComponents.month
        timeComponents.day = dateComponents.day
        return NSCalendar.currentCalendar().dateFromComponents(timeComponents)
    }
}