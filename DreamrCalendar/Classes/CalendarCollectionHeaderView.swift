//
//  CalendarCollectionHeaderView.swift
//  DreamrCalendar
//
//  Created by Work on 14/07/2016.
//  Copyright © 2016 CocoaPods. All rights reserved.
//

import UIKit

class CalendarCollectionHeaderView: UICollectionReusableView {

    //MARK: - Declarations
    
    //MARK: Outlets
    @IBOutlet weak var titleLbl: CustomFontLabel!
    
    //MARK: - Awake from nib
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
}