//
//  CalendarCollectionView.swift
//  DreamrCalendar
//
//  Created by Work on 08/07/2016.
//  Copyright © 2016 CocoaPods. All rights reserved.
//

import UIKit

protocol CalendarCollectionViewDelegate: class {
    func didSelectItem(indexPath: NSIndexPath, cell: UICollectionViewCell?, date: NSDate)
}

class CalendarCollectionView: UICollectionView {
    
    let reuseIdentifier: String = "DayCell"
    let calendar = NSCalendar.currentCalendar()
    let itemsPerPage = 7
    let headerWidth: CGFloat = 80.0
    
    var info: DreamrCalendarInfo?
    var previousSelectedIndexPath: NSIndexPath?
    var keys = Array<NSDate>()
    var dates = Dictionary<NSDate, Array<NSDate>>()
    var dateRange: CalendarRange
    var dateFormatter: NSDateFormatter
    let keyDateFormatter: NSDateFormatter
    
    weak var calendarDelegate: CalendarCollectionViewDelegate?
    
    init(dateRange: CalendarRange, info: DreamrCalendarInfo?) {
        self.dateRange = dateRange
        self.info = info
        self.dateFormatter = NSDateFormatter()
        self.dateFormatter.dateFormat = "MMM"
        self.keyDateFormatter = NSDateFormatter()
        self.keyDateFormatter.dateFormat = "MMM-yyyy"
        super.init(frame: CGRectZero, collectionViewLayout: UICollectionViewLayout())
        self.delegate = self
        self.dataSource = self
        self.setAppearance()
        self.registerNibs()
        self.buildDates()
        self.setLayout()
        self.previousSelectedIndexPath = NSIndexPath(forItem: 0, inSection: 0)
        self.selectItemAtIndexPath(NSIndexPath(forItem: 0, inSection: 0), animated: false, scrollPosition: .None)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setAppearance() {
        self.backgroundColor = UIColor.whiteColor()
        self.showsHorizontalScrollIndicator = false
        self.showsVerticalScrollIndicator = false
        self.layer.borderColor = UIColor(red: 208/255, green: 208/255, blue: 208/255, alpha: 1.0).CGColor
        self.layer.borderWidth = 1
    }
    
    func setLayout() {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .Horizontal
        self.collectionViewLayout = layout
    }
    
    func registerNibs() {
        self.registerNib(UINib(nibName: "DayCell", bundle: podBundle), forCellWithReuseIdentifier: self.reuseIdentifier)
        self.registerNib(UINib(nibName: "CalendarCollectionHeaderView", bundle: podBundle), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "CalendarCollectionHeaderView")
    }

    func buildDates() {
        
        var startingDate = self.dateRange.from
        
        self.addDate(startingDate, toData: &self.dates)
        while startingDate.compare(self.dateRange.to) != .OrderedDescending {
            
            guard let date = self.calendar.dateByAddingUnit(.Day, value: dateRange.interval, toDate: startingDate, options: []) else {
                continue
            }
            self.addDate(date, toData: &self.dates)
            startingDate = date
        }
        self.createKeys()
        self.reloadData()
    }
    
    func addDate(date: NSDate, inout toData data: Dictionary<NSDate, Array<NSDate>>) {
        let keyDateString = self.keyDateFormatter.stringFromDate(date)
        guard let dateKey = self.keyDateFormatter.dateFromString(keyDateString) else {
            return
        }
        if self.dates[dateKey] == nil {
            self.dates[dateKey] = [date]
        } else {
            self.dates[dateKey]?.append(date)
        }
    }
    
    func createKeys() {
        self.keys = Array(self.dates.keys)
        self.keys.sortInPlace { (dateOne, dateTwo) -> Bool in
//            guard let dateOne = self.dateFormatter.dateFromString(keyOne), let dateTwo = self.dateFormatter.dateFromString(keyTwo) else {
//                return false
//            }
            return dateOne.compare(dateTwo) == .OrderedAscending
        }
    }
    
    func selectCell(collectionView: UICollectionView, indexPath: NSIndexPath) {
        if let cell = collectionView.cellForItemAtIndexPath(indexPath) as? DayCell, appearanceInfo = self.info?.dateAppearanceInfo {
            cell.setSelected(appearanceInfo)
            self.previousSelectedIndexPath = indexPath
            
            let cellWidth = self.bounds.width/CGFloat(self.itemsPerPage)
            let halfItemsPerPage = Int(self.itemsPerPage/2)
            var offset: CGFloat = 0
            
            var sectionCount = 0
            var indexTotalCount = 0
            while sectionCount <= indexPath.section {
                
                if sectionCount == indexPath.section {
                    indexTotalCount += indexPath.row
                    indexTotalCount += 1
                } else { 
                    indexTotalCount += self.dates[self.keys[sectionCount]]!.count
                }
                sectionCount += 1
            }
            
            let totalSectionWidth = CGFloat(sectionCount) * self.headerWidth
            let totalCellWidth = CGFloat(indexTotalCount) * cellWidth
            
            defer {
                self.setContentOffset(CGPoint(x: offset, y: self.contentOffset.y), animated: true)
            }
            
            guard indexTotalCount > halfItemsPerPage else {
                if indexTotalCount == halfItemsPerPage {
                    offset += cellWidth/2
                }
                return
            }
            
            offset = totalSectionWidth + totalCellWidth
            offset -= CGFloat(halfItemsPerPage + 1) * cellWidth
            
            var totalCellsCount = 0
            for key in self.keys {
                totalCellsCount += self.dates[key]!.count
            }
            
            if indexTotalCount > totalCellsCount - halfItemsPerPage {
                
                if self.dates[self.keys.last!]!.count < halfItemsPerPage {
                    // problems here
                    let lastScrollIndex = totalCellsCount - self.itemsPerPage
                    offset = CGFloat(lastScrollIndex)*cellWidth + totalSectionWidth
                    
                    if indexPath.section != self.keys.count - 1  {
                        offset += self.headerWidth
                    }
                    if indexTotalCount == totalCellsCount - (halfItemsPerPage - 1) {
                        offset -= cellWidth/2
                    }
                } else {
                    let lastScrollIndex = totalCellsCount - self.itemsPerPage
                    offset = CGFloat(lastScrollIndex)*cellWidth + totalSectionWidth
                }
            }
        }
    }
    
    func deselectPreviousCell(collectionView: UICollectionView, indexPath: NSIndexPath?) {
        if let previousIndexPath = indexPath, let cell = collectionView.cellForItemAtIndexPath(previousIndexPath) as? DayCell, appearanceInfo = self.info?.dateAppearanceInfo {
            cell.setDeselected(appearanceInfo)
        }
    }
}

extension CalendarCollectionView: UICollectionViewDataSource {
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return self.dates.count
    }
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let dates = self.dates[self.keys[section]] else {
            return 0
        }
        return dates.count
    }
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(self.reuseIdentifier, forIndexPath: indexPath) as! DayCell
        guard let dates = self.dates[self.keys[indexPath.section]] else {
            return cell
        }
        var selected = false
        if let previousSelectedIndexPath = self.previousSelectedIndexPath where previousSelectedIndexPath == indexPath {
            selected = true
        }
        cell.refresh(dates[indexPath.row], info: self.info, selected: selected)
        return cell
    }
    func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        guard kind == UICollectionElementKindSectionHeader else {
            return UICollectionReusableView()
        }
        let view = collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: "CalendarCollectionHeaderView", forIndexPath: indexPath) as! CalendarCollectionHeaderView
        view.titleLbl.text = self.dateFormatter.stringFromDate(self.keys[indexPath.section])
        if let fontName = self.info?.fontPostScriptName {
            view.titleLbl.font = UIFont(name: fontName, size: 13.0)
        }
        return view
    }
}

extension CalendarCollectionView: UICollectionViewDelegateFlowLayout {
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        self.deselectPreviousCell(collectionView, indexPath: self.previousSelectedIndexPath)
        self.selectCell(collectionView, indexPath: indexPath)
        guard let dates = self.dates[self.keys[indexPath.section]] else {
            return
        }
        self.calendarDelegate?.didSelectItem(indexPath, cell: collectionView.cellForItemAtIndexPath(indexPath), date: dates[indexPath.row])
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsZero
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.width/CGFloat(self.itemsPerPage), height: collectionView.bounds.height)
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSizeMake(self.headerWidth, collectionView.bounds.height)
    }
}