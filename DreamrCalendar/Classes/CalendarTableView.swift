//
//  CalendarTableView.swift
//  DreamrCalendar
//
//  Created by Work on 08/07/2016.
//  Copyright © 2016 CocoaPods. All rights reserved.
//

import UIKit

let podBundle = NSBundle(identifier: "org.cocoapods.DreamrCalendar")

protocol CalendarTableViewDelegate: class {
    func didSelectRow(indexPath: NSIndexPath, cell: UITableViewCell?, time: NSDate)
}

class CalendarTableView: UITableView {
    
    let reuseIdentifier: String = "TimeCell"
    let calendar = NSCalendar.currentCalendar()
    let itemsPerPage = 7
    
    var info: DreamrCalendarInfo?
    var previousSelectedIndexPath: NSIndexPath?
    var times = Array<NSDate>()
    var timeRange: CalendarRange
    
    weak var calendarDelegate: CalendarTableViewDelegate?
    
    init(timeRange: CalendarRange, info: DreamrCalendarInfo?) {
        self.timeRange = timeRange
        self.info = info
        super.init(frame: CGRectZero, style: .Plain)
        self.delegate = self
        self.dataSource = self
        self.setAppearance()
        self.registerCell()
        self.buildTimes()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setAppearance() {
        self.backgroundColor = UIColor.clearColor()
        self.showsHorizontalScrollIndicator = false
        self.showsVerticalScrollIndicator = false
        self.separatorStyle = UITableViewCellSeparatorStyle.None
    }
    
    func registerCell() {
        self.registerNib(UINib(nibName: "TimeCell", bundle: podBundle), forCellReuseIdentifier: self.reuseIdentifier)
    }
    
    func buildTimes() {
        
        var startingDate = self.timeRange.from

        self.times.append(startingDate)
        while startingDate.compare(self.timeRange.to) != .OrderedDescending {
            
            guard let date = self.calendar.dateByAddingUnit(.Minute, value: self.timeRange.interval, toDate: startingDate, options: []) else {
                continue
            }
            self.times.append(date)
            startingDate = date
        }
        
        self.reloadData()
    }
    
    func selectCell(tableView: UITableView, indexPath: NSIndexPath) {
        guard let cell = tableView.cellForRowAtIndexPath(indexPath) as? TimeCell, appearanceInfo = self.info?.timeAppearanceInfo else {
            return
        }
        cell.setSelected(true, appearanceInfo: appearanceInfo, animated: true)
        self.previousSelectedIndexPath = indexPath
    }

    func deselectPreviousCell(tableView: UITableView, indexPath: NSIndexPath?) {
        guard let previousIndexPath = indexPath, let cell = tableView.cellForRowAtIndexPath(previousIndexPath) as? TimeCell, appearanceInfo = self.info?.timeAppearanceInfo else {
            return
        }
        cell.setSelected(false, appearanceInfo: appearanceInfo, animated: true)
    }
}

extension CalendarTableView: UITableViewDataSource {
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.times.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(self.reuseIdentifier) as! TimeCell
        cell.refresh(self.times[indexPath.row], info: self.info)
        return cell
    }
}

extension CalendarTableView: UITableViewDelegate {
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.deselectPreviousCell(tableView, indexPath: self.previousSelectedIndexPath)
        self.selectCell(tableView, indexPath: indexPath)
        guard let cell = tableView.cellForRowAtIndexPath(indexPath) where self.times.count > indexPath.row else {
            return
        }
        self.calendarDelegate?.didSelectRow(indexPath, cell: cell, time: self.times[indexPath.row])
    }
}