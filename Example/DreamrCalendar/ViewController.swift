//
//  ViewController.swift
//  DreamrCalendar
//
//  Created by Ted on 07/07/2016.
//  Copyright (c) 2016 Ted. All rights reserved.
//

import UIKit
import DreamrCalendar

class ViewController: UIViewController {

    var calendarView: DreamrCalendarView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let toDate = NSCalendar.currentCalendar().dateByAddingUnit(.Day, value: 250, toDate: NSDate(), options: [])!
        let toTime = NSCalendar.currentCalendar().dateByAddingUnit(.Hour, value: 12, toDate: NSDate(), options: [])!

        let dateAppearanceInfo = DreamrCalendarAppearanceInfo(selectedTextColor: UIColor.blackColor(), deselectedTextColor: UIColor.lightGrayColor())
        let timeAppearanceInfo = DreamrCalendarAppearanceInfo(selectedTextColor: UIColor.blackColor(), deselectedTextColor: UIColor.lightGrayColor())

        let info = DreamrCalendarInfo(dateAppearanceInfo: dateAppearanceInfo, timeAppearanceInfo: timeAppearanceInfo, fontPostScriptName: "GalebRegular")
        
        self.calendarView = DreamrCalendarView(dateRange: (from: NSDate(), to: toDate, interval: 1), timeRange: (from: NSDate(), to: toTime, interval: 30), info: info)
        self.calendarView.delegate = self
        self.calendarView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(self.calendarView)
        
        let views = ["calendarView": self.calendarView]
        self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[calendarView]|", options: [], metrics: nil, views: views))
        self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-20-[calendarView]|", options: [], metrics: nil, views: views))
        
        self.view.backgroundColor = UIColor(red: 236/255, green: 236/255, blue: 236/255, alpha: 1.0)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension ViewController: DreamrCalendarViewDelegate {
    func didSelectDateItem(indexPath: NSIndexPath, cell: UICollectionViewCell?, date: NSDate) {
        print("didSelectDate = ", date)
    }
    func didSelectTimeRow(indexPath: NSIndexPath, cell: UITableViewCell?, dateTime time: NSDate) {
        print("didSelectTimeRow = ", time)
    }
}