# DreamrCalendar

[![CI Status](http://img.shields.io/travis/Ted/DreamrCalendar.svg?style=flat)](https://travis-ci.org/Ted/DreamrCalendar)
[![Version](https://img.shields.io/cocoapods/v/DreamrCalendar.svg?style=flat)](http://cocoapods.org/pods/DreamrCalendar)
[![License](https://img.shields.io/cocoapods/l/DreamrCalendar.svg?style=flat)](http://cocoapods.org/pods/DreamrCalendar)
[![Platform](https://img.shields.io/cocoapods/p/DreamrCalendar.svg?style=flat)](http://cocoapods.org/pods/DreamrCalendar)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

DreamrCalendar is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "DreamrCalendar"
```

## Author

Ted, tl@dreamr.uk

## License

DreamrCalendar is available under the MIT license. See the LICENSE file for more info.
